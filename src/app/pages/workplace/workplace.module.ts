import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WorkplaceRoutingModule } from './workplace-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    WorkplaceRoutingModule
  ]
})
export class WorkplaceModule { }
